export const spinner = parent => {
    const loader = `
            <div class="spinner-border" style="width: 4rem; height: 4rem;" role="status">
                <span class="sr-only"></span>
            </div>
    `;
    parent.insertAdjacentHTML('afterbegin', loader);
};

export const deleteSpinner = (parent) => {
    parent.innerHTML = '';
};
