import elements from "./elements";

const renderSome = (someChk) => {
    return (someChk === true )
        ? `<h3><i class="far fa-smile"></i></h3>`
        : (someChk === false )
        ? `<h3><i class="far fa-frown"></i></h3>`
        : `<h3><i class="fas fa-pen"></i></h3>`;

};

export const renderPhone = ({phoneChk}) => {
    elements.checkPhone.innerHTML = renderSome(phoneChk);
};

export const renderPassword = ({passwordChk}) => {
    elements.checkPassword.innerHTML = renderSome(passwordChk);
};

export const renderEmail = ({emailChk}) => {
    elements.checkEmail.innerHTML = renderSome(emailChk);
};

export const renderSubmit = ({submitButtonDisabled}) => {
    elements.submit.disabled = submitButtonDisabled;
};

