const elements = {
    spinner: document.querySelector('.spinner'),
    beers: document.querySelector('.beers'),
    pages: document.querySelector('.pages'),
    sort: document.querySelector('.sort'),
    favorites: document.querySelector('.favorites'),
    modalPage: document.querySelector('#btnModal'),
    modal: document.querySelector('.exampleModal'),
    btnClose: document.querySelector('#btnClose'),
    checkPhone: document.querySelector('.check_phone'),
    checkEmail: document.querySelector('.check_email'),
    checkPassword: document.querySelector('.check_password'),
    inputPhone: document.querySelector('#inputPhone'),
    inputEmail: document.querySelector('#inputEmail'),
    inputPassword: document.querySelector('#inputPassword'),
    submit: document.querySelector('#btnSubmit')
};

export default elements;