import elements from "./elements";

const renderPages = (page, lastPage) => {
    elements.pages.innerHTML  = `
        ${page > 1 
        ? `<a href="#${page - 1}" class="col-3 badge badge p-2">Previously</a>` 
        : `<div class="col-3"> </div>`}
        <div class="col-4 text-center">Page ${page} / ${lastPage}</div>
        ${page < lastPage 
        ? `<a href="#${page + 1}" class="col-3 badge p-2 ">Next</a>` 
        : ''}
    `;

};

export default renderPages;

