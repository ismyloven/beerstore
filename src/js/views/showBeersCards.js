import elements from "./elements";

const renderBeer = (beer) => {
    const {id, image_url, name, contributed_by,
        first_brewed, ibu, abv, description,favorites} = beer;
    return `
        <div class="col-md-3 card border-light m-3">
            <div class="container mt-3">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <img src="${image_url}" alt="${name}" class="image_beer">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <ul class="list-group">
                    <li class="list-group-item">Provider: ${contributed_by}</li>
                    <li class="list-group-item">First brewed: ${first_brewed}</li>
                    <li class="list-group-item">Bitterness IBU: ${ibu}</li>
                    <li class="list-group-item">Alcohol  ${abv}%</li>
                </ul>
                <h6 class="mt-2">Description</h6>
                <div>
                    <p class="card-text p-2">${description}</p>
                </div>
                <button data-favorites ="${id}" class="btn btn-block btn btn-secondary">
                ${(!favorites)
                    ?'<i class="far fa-heart"></i>'
                    :'<i class="fas fa-heart"></i>'}
                Add as favorites</button>
            </div>
        </div> `
    };

const clearCard = () => {
    elements.beers.innerHTML = '';
};

const renderCards = (beers, page = 1, amount = 4) => {
    clearCard();

    const firstIndex = (page - 1) * amount;
    const lastIndex = page * amount - 1;
    beers = beers.slice(firstIndex, lastIndex + 1);

    const html = beers.map(beer => renderBeer(beer)).join('');
    elements.beers.insertAdjacentHTML('afterbegin', html);
};

export default renderCards;


