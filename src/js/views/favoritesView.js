import elements from "./elements";

const renderFavorite = (beer) => {
    const {id, name} = beer;
    return `
            <li class="list-group-item"  style= 'font-size:14px;'>
                ${name} <h6 class="float-right text-secondary">
                <i data-favorites="${id}" class="fas fa-trash-alt ml-2" style="cursor: pointer"></i></h6>
            </li>
    `;
};

const deleteFavoritesButton = () => {
  return `
        <button data-clear="clear" class="btn btn-outline-danger btn-bkg">
        Clear all
        </button>
  `;
};

export const clearFavorites = () => {
    elements.favorites.innerHTML = '';
};

export const renderFavorites = (beers) => {
    clearFavorites();
    beers = beers.filter(el => el.favorites);
    let html = beers.map((beer) => renderFavorite(beer)).join('');

    if (beers.length){
        html += deleteFavoritesButton();
    }

    elements.favorites.insertAdjacentHTML('beforeend', html);
};

