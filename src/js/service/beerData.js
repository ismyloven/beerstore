import beerApi from './beerApi';

export default class BeersApi {
	getBeer() {
		return beerApi.get()
			.then(({data}) => {
				 // console.log(data);
				this.data = data;
			})
			.catch(e => console.log(e.stack));

	}

}
