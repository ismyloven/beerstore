import {api} from "../config";
import axios from 'axios';

export default axios.create({
	baseURL: api,
})

