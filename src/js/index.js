import BeersApi from "./service/beerData";
import { checkDataInLS, checkIdLS, clearAllData, setLocalStorageName, toggle } from './models/LocalStorage';
import {modalObj, setEmail, setMinLength, setPassword, setPhone } from './models/Modal';

import elements from "./views/elements";
import { deleteSpinner, spinner } from "./views/showLoader";
import renderCards from "./views/showBeersCards";
import renderPages from './views/showPages';
import { clearFavorites, renderFavorites } from "./views/favoritesView";
import { renderEmail, renderPassword, renderPhone, renderSubmit } from "./views/modalView";


const state = {
    pages: {
        page: 1,
        amount: 3,
        lastPage: 1
    },

};
setLocalStorageName('beers');
setMinLength(6,7);


function allPipe(item, ...rest) {
    rest.forEach((func) => {
        func(item);
    });
}

function runModalPipe(e, method) {
    switch (method) {
        case 'phone':
            setPhone(e.target.value);
            break;
        case 'email':
            setEmail(e.target.value);
            break;
        case 'password':
            setPassword(e.target.value);
            break;
        default: console.log('Wrong argument: ' + method);
    }

    allPipe(modalObj, renderPhone, renderEmail, renderPassword, renderSubmit);
}

elements.inputPhone.addEventListener('keyup', (e) => {
    runModalPipe(e,'phone');
});

elements.inputEmail.addEventListener('keyup', (e) => {
    runModalPipe(e, 'email');
});

elements.inputPassword.addEventListener('keyup', (e) => {
    runModalPipe(e, 'password');
});
function runRenderCard() {
    renderCards(state.beers.data, state.pages.page, state.pages.amount);
}

window.addEventListener('load', () => {

    spinner(elements.spinner);
    state.beers = new BeersApi();
    state.beers.getBeer()
        .then(() => {
            const length = state.beers.data.length;
            state.pages.lastPage = Math.ceil(length / state.pages.amount);

            const newPage = parseInt(location.hash.slice(1));
            if ((!isNaN(newPage)) && (newPage >= 1) && (newPage <= state.pages.lastPage)) {
                state.pages.page = newPage;
            }

            updateFavorites();
            deleteSpinner(elements.spinner);
            runRenderCard();
            renderPages(state.pages.page, state.pages.lastPage);
            renderFavorites(state.beers.data);

        })
        .catch(error => console.error('error data', error));
});

let elemArray = [elements.beers, elements.favorites];

elemArray.forEach(elem => {
    elem.addEventListener('click', handleFavorites)
});

function handleFavorites(e) {
    const target = e.target.closest('[data-favorites]');
    if (target !== null) {
        const idFavorites = +target.dataset.favorites;
        toggle(idFavorites);
        updateFavorites();
        runRenderCard();
        renderFavorites(state.beers.data);
    }
}


elements.sort.addEventListener('click', handleSort)


function handleSort(e) {
    const target = e.target.closest('[data-sort_abv]') || e.target.closest('[data-sort_ibu]');
    if (target !== null) {
        const sort_abv = target.dataset.sort_abv;
        const sort_ibu = target.dataset.sort_ibu;

        if (sort_abv) {
            state.beers.data.sort((a, b) => {
                return (sort_abv === 'up')
                    ? b.abv - a.abv
                    : a.abv - b.abv;
            });
        } else if (sort_ibu) {
            state.beers.data.sort((a, b) => {
                return (sort_ibu === 'up')
                    ? b.ibu - a.ibu
                    : a.ibu - b.ibu
            });
        }
        runRenderCard();
    }
}


elements.favorites.addEventListener('click', handleClear);

function handleClear(e) {
    if (e.target.dataset.clear) {
        clearFavorites();
        clearAllData();
        state.beers.data.forEach(el => {
            el.favorites = false;
        });
        runRenderCard();
    }
}


window.addEventListener('hashchange', handleRender);

function handleRender() {
    const newPage = parseInt(location.hash.slice(1));
    if ((!isNaN(newPage)) && (newPage >= 1) && (newPage <= state.pages.lastPage)) {
        state.pages.page = newPage;
        renderCards(state.beers.data, newPage, state.pages.amount);
        renderPages(newPage, state.pages.lastPage);
        renderFavorites(state.beers.data);
    }
}

elements.modalPage.addEventListener('click', (e) => {
    elements.modal.style.display = 'block';
});

window.addEventListener('click', (e) => {
    if ((e.target === elements.modal) || (e.target.dataset.modal === 'close')) {
        elements.modal.style.display = 'none';
    }
});

function updateFavorites() {
    if (checkDataInLS()) {
        state.beers.data.forEach((el) => {
            el.favorites = !!checkIdLS(el.id);
        });
    }
}