export const modalObj = {
	minLenPhone: 5,
	minLenPassword: 5,
	phoneChk: '',
	passwordChk: '',
	emailChk: '',
	phone: '',
	password: '',
	email: '',
	submitButtonDisabled: true,
}


export function setMinLength(minLenPhone, minLenPassword) {
	modalObj.minLenPhone = minLenPhone;
	modalObj.minLenPassword = minLenPassword;
}

export function setPhone(phone) {
	modalObj.phone = phone;
	checkPhone();
	setSubmitButton()
}

export function setEmail(email) {
	modalObj.email = email;
	checkEmail();
	setSubmitButton()
}

export function setPassword(password) {
	modalObj.password = password;
	checkPassword();
	setSubmitButton()
}

function checkPhone() {
	if (modalObj.phone === '') {
		modalObj.phoneChk = false;
		return false;
	}
	const pattern = new RegExp(`[0-9]{${modalObj.minLenPhone},}`, '');
	modalObj.phoneChk = pattern.test(modalObj.phone);
}

function checkPassword() {
	if (modalObj.password === '') {
		modalObj.passwordChk = false;
		return false;
	}
	modalObj.passwordChk = modalObj.password.trim().length >= modalObj.minLenPassword;
}

function checkEmail() {
	if (modalObj.email === '') {
		modalObj.emailChk = false;
		return false;
	}
	const pattern = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
	modalObj.emailChk = pattern.test(modalObj.email);
}

function setSubmitButton() {
	modalObj.submitButtonDisabled = !(modalObj.phoneChk && modalObj.passwordChk && modalObj.emailChk);
}

