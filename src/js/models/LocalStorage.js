let lsName;

export function setLocalStorageName(name) {
	lsName = name;
}

export function getDataFromLS() {
	if (localStorage.getItem(lsName) !== null) {
		return JSON.parse(localStorage.getItem(lsName));
	}
	return false;
}

export function addDataToLS(id) {
	if (!checkDataInLS()) {
		localStorage.setItem(lsName, JSON.stringify([id]));

	} else {
		let beers = getDataFromLS();
		if (!beers.includes(id)) {
			beers.push(id);
			localStorage.setItem(lsName, JSON.stringify(beers));
		}
	}
}

export function deleteDataFromLS(id) {
	let beers = getDataFromLS();
	const i = beers.findIndex((el) => el === id);
	if (i !== -1) {
		beers.splice(i, 1);
	}
	localStorage.setItem(lsName, JSON.stringify(beers));
}

export function checkDataInLS() {
	return !!localStorage.getItem(lsName);
}

export function clearAllData() {
	localStorage.removeItem(lsName);
}

export function checkIdLS(id) {
	if (checkDataInLS()) {
		const beers = getDataFromLS();
		return beers.includes(id);
	}
	return false
}

export function toggle(id) {
	checkIdLS(id)
		? deleteDataFromLS(id)
		: addDataToLS(id)
}

